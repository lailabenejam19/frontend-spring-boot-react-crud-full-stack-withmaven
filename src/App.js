import React, { Component } from 'react';
import './App.css';
import InstructorApp from './component/InstructorApp';
import NavBarApp from './component/Paginas/NavBarApp';


class App extends Component {
    render() {
        return (

            <div>
                <NavBarApp />
                <InstructorApp />
            </div>

            );
    }
}

export default App;
