import axios from 'axios'
const INSTRUCTOR_API_URL = 'http://localhost:8080'

class InstructorDataService {

    retrieveAllInstructors() {
        return axios.get(`${INSTRUCTOR_API_URL}/instructors`);
    }

    retrieveInstructor(id) {
        return axios.get(`${INSTRUCTOR_API_URL}/instructors/${id}`);
    }

    retrieveAllInstructorsByUsername(username) {
        console.log("log byuser -" + username)
        return axios.get(`${INSTRUCTOR_API_URL}/{username}`);
    }

    deleteInstructor(id) {
        return axios.delete(`${INSTRUCTOR_API_URL}/instructors/${id}`);
    }

    updateInstructor(id, instructor) {
        return axios.put(`${INSTRUCTOR_API_URL}/instructors/${id}`, instructor);
    }

    createInstructor(instructor) {
        return axios.post(`${INSTRUCTOR_API_URL}/instructors`, instructor);
    }

}

export default new InstructorDataService()