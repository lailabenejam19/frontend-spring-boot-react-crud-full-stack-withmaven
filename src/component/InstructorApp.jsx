import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ListCoursesComponent from './Courses/ListCoursesComponent';
import ListCoursesByUserComponent from './Courses/ListCoursesByUserComponent';
import CourseComponent from './Courses/CourseComponent';
import InstructorComponent from './Instructors/InstructorComponent';
import Home from './Paginas/Home';
import ListInstructorsComponent from './Instructors/ListInstructorsComponent';


class InstructorApp extends Component {

 render() {
     return (
         //Uso de enrutador, conmutador y ruta
         <BrowserRouter>
             <>
                 <Switch>
                     <Route path="/" exact component=
                         {Home} />
                     <Route path="/courses" exact component=
                         {ListCoursesComponent} />
                     <Route path="/instructors" exact component=
                         {ListInstructorsComponent} />
                     <Route path="/courses/:id" exact component=
                         {CourseComponent} />
                     <Route path="/instructors/:id" exact component=
                         {InstructorComponent} />
                     <Route path="/instructors/:username/courses" exact component=
                         {ListCoursesByUserComponent} />

                 </Switch>
             </>
         </BrowserRouter>
     )
 }
}

//Exportar para utilizarlos en otros m�dulos
export default InstructorApp