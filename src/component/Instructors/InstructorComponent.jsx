import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import InstructorDataService from '../../service/InstructorDataService';

class InstructorComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            name: '',
            lastname:'',
            username: ''
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)
        this.backClicked = this.validate.bind(this)

    }
    componentDidMount() {
        console.log(this.state.id)
        // eslint-disable-next-line
        if (this.state.id == -1) {
            return
        }

        InstructorDataService.retrieveInstructor(this.state.id)
            .then(response => this.setState({
                name: response.data.name,
                lastname: response.data.lastname,
                username: response.data.username
            }))
    }

    backClicked() {
        this.props.history.push(`/`)
    }

    onSubmit(values) {
        let instructor = {
            id: this.state.id,
            name: values.name,
            lastname: values.lastname,
            username: values.username,
            // targetDate: values.targetDate
        }

        if (this.state.id === '-1') {
            InstructorDataService.createInstructor(instructor)
                .then(() => this.props.history.push('/instructors'))
        } else {
            InstructorDataService.updateInstructor(this.state.id,
                instructor)
                .then(() => this.props.history.push('/instructors'))
        }
        console.log(values);
    }

    
    
    validate(values) {
        let errors = {}
        if (!values.name) {
            errors.name = 'Enter a Name'
        } else if (values.name.length < 3) {
            errors.name = 'Enter atleast 3 Characters in Name'
        }
        return errors
    }

    render() {
        let { name, lastname, username, id } = this.state
        return (
            <div>
                <blockquote className="blockquote text-center">
                    <br></br>
                    <h3>New Instructor</h3>
                    <br></br>
                </blockquote>
                <div className="container">
                    <Formik
                        initialValues={{ id, name, lastname, username }}
                        onSubmit={this.onSubmit} validateOnChange={false}
                        validateOnBlur={false}
                        validate={this.validate}
                        enableReinitialize={true}>
                        {
                            (props) => (
                                <Form>
                                    <fieldset className="form-group">
                                        <label>Id</label>
                                        <Field className="form-control" type="text" name="id" disabled />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>Last Name</label>
                                        <Field className="form-control" type="text" name="lastname" />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>Name</label>
                                        <Field className="form-control" type="text" name="name" />
                                        <ErrorMessage name="name" component="div" className="alert alert-warning" />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>Username</label>
                                        <Field className="form-control" type="text" name="username" />
                                    </fieldset>
                                    <button className="btn btn-success" type="submit">Save</button>
                                </Form>
                            )
                        }
                    </Formik>
                </div>
            </div>

        )
    }

}
export default InstructorComponent