import React, { Component } from 'react';
import InstructorDataService from '../../service/InstructorDataService';

class ListInstructorsComponenet extends Component {

    constructor(props) {
        super(props)
        this.state = {
            instructors: [],
            textBuscar: '',
            searchInstructor: '',
            instructorBackup: [],
            message: null
        }

        this.refreshInstructor = this.refreshInstructor.bind(this)
        this.updateInstructorClicked = this.updateInstructorClicked.bind(this)
        this.addInstructorClicked = this.addInstructorClicked.bind(this)
        this.deleteInstructorClicked = this.deleteInstructorClicked.bind(this)
        this.cursosInstructorClicked = this.cursosInstructorClicked.bind(this)
        this.backClicked = this.backClicked.bind(this)

    }

    componentDidMount() {
        this.refreshInstructor();
    }

    cursosInstructorClicked(username) {
        this.props.history.push(`/instructors/${username}/courses`)
    }

    updateInstructorClicked(id) {
        console.log('updateInstructor' + id)
        this.props.history.push(`/instructors/${id}`)
    }

    backClicked() {
        this.props.history.push(`/`)
    }

    
    refreshInstructor() {
        InstructorDataService.retrieveAllInstructors()//HARDCODED
            .then(
                response => {
                    console.log(response);
                    this.setState({
                        instructors: response.data,
                        instructorBackup: response.data
                    })
                }
            )
    }

    addInstructorClicked() {
        this.props.history.push(`/instructors/-1`)
    }


    deleteInstructorClicked(id) {
       InstructorDataService.deleteInstructor(id)
            .then(
                response => {
                    this.setState({
                        message: `Delete of instructors ${id} Successful`
                    })
                    this.refreshInstructor(this.state.username)
                }
            )
    }

    filter(event) {
        var searchInstructor = event.target.value
        const data = this.state.instructorBackup
        const newData = data.filter(function (item) {
            const itemLastname = item.lastname.toUpperCase()
            const itemName = item.name.toUpperCase()
            const itemUsername = item.username.toUpperCase()
            const campo = itemLastname + " " + itemName + itemUsername
            const textData = searchInstructor.toUpperCase()
            return campo.indexOf(textData) > -1
        })
        this.setState({
            instructors: newData,
            searchInstructor: searchInstructor,
        })
    }

    render() {
        console.log('render')
        return (
            <div className="container">
                <blockquote className="blockquote text-center">
                    <br></br>
                    <h3>All Instructors</h3>
                    <div className="float-right col-md-1">
                        <button className="btn btn-success" onClick={this.addInstructorClicked}>Add</button>
                    </div>
                    <div className="float-right">
                        <form className="form-inline my-2 my-lg-0">
                            <input className="form-control mr-sm-2" placeholder="Search" value={this.state.searchInstructor} onChange={(searchInstructor) => this.filter(searchInstructor)} />
                        </form>
                    </div>
                    <br></br>
                </blockquote>
                <div className="container">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Last Name</th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Courses</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            { this.state.instructors.length > 0 &&
                                this.state.instructors.sort(({ id: prevId }, { id: currId }) => prevId - currId).map(
                                    instructor =>
                                        <tr key={instructor.id}>
                                            <td> {instructor.id} </td>
                                            <td> {instructor.lastname} </td>
                                            <td> {instructor.name} </td>
                                            <td> {instructor.username} </td>
                                            <td><button className="btn btn-primary" onClick={() => this.cursosInstructorClicked(instructor.username)}>Courses</button></td>
                                            <td><button className="btn btn-success" onClick={() => this.updateInstructorClicked(instructor.id)}>Update</button></td>
                                            <td><button className="btn btn-warning" onClick={() => this.deleteInstructorClicked(instructor.id, instructor.username)}>Delete</button></td>
                                        </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>

                <div className="form-group row">
                    <div className="col-md-1">
                        <button className="btn btn btn-danger" onClick={this.backClicked}>Back</button>
                    </div>
                </div>


            </div>
            )
    }

}

export default ListInstructorsComponenet