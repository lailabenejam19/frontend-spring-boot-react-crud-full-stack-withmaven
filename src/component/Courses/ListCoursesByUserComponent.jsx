import React, { Component } from 'react';
import CourseDataService from '../../service/CourseDataService';
import InstructorDataService from '../../service/InstructorDataService';


class ListCoursesComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            username: this.props.match.params.username,
            courses: [],
            message: null
        }

        this.refreshCourses = this.refreshCourses.bind(this)
        this.updateCourseClicked = this.updateCourseClicked.bind(this)
        this.addCourseClicked = this.addCourseClicked.bind(this)
        this.deleteCourseClicked = this.deleteCourseClicked.bind(this)
        this.backClicked = this.backClicked.bind(this)

    }

    componentDidMount() {
        this.refreshCourses(this.state.username);

    }

    refreshCourses(username) {
        CourseDataService.retrieveAllByUsername(username)//HARDCODED
            .then(
                response => {
                    this.setState({
                        courses: response.data
                    })
                    console.log(this.state.courses)
                }
            )
    }

    updateCourseClicked(id) {
        console.log('updateCourse' + id)
        this.props.history.push(`/courses/${id}`)
    }

    backClicked() {
        this.props.history.push(`/instructors`)
    }


    addCourseClicked() {
        this.props.history.push(`/courses/-1`)
    }


    deleteCourseClicked(username) {
        InstructorDataService.deleteInstructor(username)
            .then(
                response => {
                    this.setState({
                        message: `Delete of course ${username} Successful`
                    })
                    this.refreshCourses()
                }
            )
    }



    render() {
        console.log('render')
        return (
            <div className="container">
                <blockquote className="blockquote text-center">
                    <br></br>
                    <h3>Courses</h3>
                    <div className="float-right col-md-1">
                        <button className="btn btn-success" onClick={this.addInstructorClicked}>Add</button>
                    </div>
                    <br></br>
                </blockquote>

                {this.state.message && <div className="alert alert-success">{this.state.message}</div>}
                <div className="container">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Instructor</th>
                                <th>Description</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {   this.state.courses.length > 0 &&
                                this.state.courses.map(
                                    course => (
                                        <tr key={course.id}>
                                            <td> {course.id} </td>
                                            <td> {course.username} </td>
                                            <td> {course.description} </td>
                                            <td><button className="btn btn-success" onClick={() => this.updateCourseClicked(course.id)}>Update</button></td>
                                            <td><button className="btn btn-warning" onClick={() => this.deleteCourseClicked(course.id)}>Delete</button></td>
                                        </tr>
                                    ))
                            }
                        </tbody>
                    </table>
                </div>
                <div className="form-group row">
                    <div className="col-md-1">
                        <button className="btn btn btn-danger" onClick={this.backClicked}>Back</button>
                    </div>
                </div>
            </div>

        )
    }
}
export default ListCoursesComponent