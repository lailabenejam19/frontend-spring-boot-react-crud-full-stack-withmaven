import React, {Component} from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import CourseDataService from '../../service/CourseDataService';
import InstructorDataService from '../../service/InstructorDataService';

class CourseComponent extends Component {

    constructor(props) {
        super(props)
       this.state = {
            id: this.props.match.params.id,
           description: '',
           username: '',
          instructor:[]
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)
   }
    componentDidMount() {
        console.log(this.state.id)

        InstructorDataService.retrieveAllInstructors()
            .then(response =>
                this.setState({
                    instructor: response.data
                }))
        console.log(this.state.instructor)


        // eslint-disable-next-line
        if (this.state.id == -1) {
            return
        }

     CourseDataService.retrieveCourse(this.state.id)
            .then(response => this.setState({
                description: response.data.description,
                username: response.data.username,
                id_instructor: response.data.username
            }))

    }

    onSubmit(values) {
        let course = {
            id: this.state.id,
            username: values.username,
            description: values.description,
           // targetDate: values.targetDate
        }

        if (this.state.id === '-1') {
            CourseDataService.createCourse(course,this.state.id_instructor)
                .then(() => this.props.history.push('/courses'))
        } else {
            CourseDataService.updateCourse(this.state.id,
                course)
                .then(() => this.props.history.push('/courses'))
        }
        console.log(values);
    }
    validate(values) {
        let errors = {}
        if (!values.description) {
            errors.description = 'Enter a Description'
        } else if (values.description.length < 5) {
            errors.description = 'Enter atleast 5 Characters in Description'
        }
        return errors
    }

    render() {
        let { description, username, id } = this.state
        return (
            <div>
                <h3>Course</h3>
                <div className="container">
                    <Formik
                        initialValues={{ id, username, description }}
                        onSubmit={this.onSubmit} validateOnChange={false}
                        validateOnBlur={false}
                        validate={this.validate}
                        enableReinitialize={true}>
                        {
                            (props) => (
                                <Form>
                                    <fieldset className="form-group">
                                        <label>Id</label>
                                        <Field className="form-control" type="text" name="id" disabled />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>Instructor</label>
                                        <Field as="select" className="form-control" name="username">
                                            <option value="0"> Seleccionar </option>
                                            {
                                                this.state.instructor.map(
                                                    instructor =>(
                                                        <option key={instructor.id} value={instructor.id}>{instructor.name} {instructor.lastname} ({instructor.username})</option>
                                                ))
                                            }
                                        </Field>
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>Description</label>
                                        <Field className="form-control" type="text" name="description" />
                                        <ErrorMessage name="description" component="div" className="alert alert-warning" />
                                    </fieldset>
                                    <button className="btn btn-success" type="submit">Save</button>
                                </Form>
                            )
                        }
                    </Formik>
                </div>
            </div>
        )
    }
}
export default CourseComponent