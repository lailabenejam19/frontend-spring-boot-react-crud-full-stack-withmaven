import React, { Component } from 'react';
import CourseDataService from '../../service/CourseDataService';

class ListCoursesComponent extends Component {


    constructor(props) {
        super(props)
        this.state = {
            courses: [],
            intructor: [],
            textBuscar: '',
            searchCourse: '',
            coursesBackup: [],
            message:null
        }

        this.refreshCourses = this.refreshCourses.bind(this)
        this.updateCourseClicked = this.updateCourseClicked.bind(this)
        this.addCourseClicked = this.addCourseClicked.bind(this)
        this.deleteCourseClicked = this.deleteCourseClicked.bind(this)
        this.backClicked = this.backClicked.bind(this)
       
    }

    componentDidMount() {
        this.refreshCourses();

    }

    updateCourseClicked(id) {
        console.log('updateCourse' + id)
        this.props.history.push(`/courses/${id}`)
    }

    backClicked() {
        this.props.history.push(`/`)
    }

    refreshCourses() {
        CourseDataService.retrieveAllCourses()//HARDCODED
            .then(
                response => {
                    console.log(response);
                    this.setState({
                        courses: response.data,
                        coursesBackup: response.data
                    })
                   
                }
        )
        console.log(this.state.intructor)
    }

    filter(event) {
        var searchCourse = event.target.value
        const data = this.state.coursesBackup
        const newData = data.filter(function (item) {
            const itemUsername = item.username
            const itemDescription = item.description.toUpperCase()
            const campo = itemUsername + " " + itemDescription
            const textData = searchCourse.toUpperCase()
            return campo.indexOf(textData) > -1
        })
        this.setState({
            courses: newData,
            searchCourse: searchCourse,
        })
    }

    addCourseClicked() {
        this.props.history.push(`/courses/-1`)
    }


    deleteCourseClicked(id) {
        CourseDataService.deleteCourse(id)
            .then(
                response => {
                    this.setState({
                        message: `Delete of course ${id} Successful` })
                    this.refreshCourses(this.state.id)
                }
            )
    }



    render() {
        console.log('render')
        return (
            <div className="container">
                <blockquote className="blockquote text-center">
                    <br/>
                    <h3>All Courses</h3>
                    <div className="float-right col-md-1">
                        <button className="btn btn-success" onClick={this.addCourseClicked}>Add</button>
                    </div>
                    <div className="float-right form-inline my-2 my-lg-0">
                        <input className="form-control mr-sm-2" placeholder="Search" value={this.state.searchCourse} onChange={(searchCourse) => this.filter(searchCourse)} />
                    </div>
                    <br/>
                </blockquote>
                
                {this.state.message && <div className="alert alert-success">{this.state.message}</div>}
                <div className="container">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Username Instructor</th>
                                <th>Instructor</th>
                                <th>Description</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            { this.state.courses.length>0 &&
                                this.state.courses.sort(({ id: prevId }, { id: currId }) => prevId - currId).map(
                                    course => (
                                        <tr key={course.id}>
                                            <td> {course.id} </td>
                                            <td> {course.username} </td>
                                            <td> {course.instructorFullName} </td>
                                            <td> {course.description} </td>
                                            <td><button className="btn btn-success" onClick={() => this.updateCourseClicked(course.id)}>Update</button></td>
                                            <td><button className="btn btn-warning" onClick={() => this.deleteCourseClicked(course.id)}>Delete</button></td>
                                        </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
                <div className="form-group row">
                    <div className="col-md-1">
                        <button className="btn btn btn-danger" onClick={this.backClicked}>Back</button>
                    </div>
                </div>
            </div>

        )
    }
}
export default ListCoursesComponent